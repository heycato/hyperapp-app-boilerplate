import { h, app } from 'hyperapp'
import view from './views'
import state from './state'
import actions from './actions'

window.onload = () =>
	app({
		view,
		state,
		actions
	})
